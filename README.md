# Quill Delta implementation

This is a Java implementation of the [QuillJs](https://quilljs.com)'s [delta](https://quilljs.com/docs/delta/).

### Build

Requirements:

 - Apache Maven 3.3.9+
 - Java 1.8

```
mvn clean package
```

