/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.embed;

import elemental.json.JsonObject;
import org.jetbrains.annotations.NotNull;

public class CommonEmbedFactory implements EmbedFactory<CommonEmbed> {

    @Override
    public boolean isApplicable(@NotNull JsonObject jsonObject) {
        return true;
    }

    @Override
    public @NotNull Class<CommonEmbed> getEmbedType() {
        return CommonEmbed.class;
    }

    @NotNull
    @Override
    public CommonEmbed createEmbed(@NotNull JsonObject jsonObject) {
        CommonEmbed commonEmbed = new CommonEmbed();
        commonEmbed.setCommonEmbedObject(jsonObject);
        return commonEmbed;
    }

    @NotNull
    @Override
    public JsonObject createJson(@NotNull CommonEmbed embed) {
        return embed.getCommonEmbedObject();
    }
}
