/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.embed;

import elemental.json.Json;
import elemental.json.JsonObject;
import org.jetbrains.annotations.NotNull;

public class ImageEmbedFactory implements EmbedFactory<Image> {

    @Override
    public boolean isApplicable(@NotNull JsonObject jsonObject) {
        return jsonObject.hasKey("image");
    }

    @Override
    public @NotNull Class<Image> getEmbedType() {
        return Image.class;
    }

    @NotNull
    @Override
    public Image createEmbed(@NotNull JsonObject jsonObject) {
        Image image = new Image();
        image.setValue(jsonObject.getString("image"));
        return image;
    }

    @Override
    public @NotNull JsonObject createJson(@NotNull Image embed) {
        JsonObject object = Json.createObject();
        object.put("image", embed.getValue());
        return object;
    }
}
