/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import io.devbench.quilldelta.exception.DeltaRendererException;
import io.devbench.quilldelta.renderer.DeltaRenderer;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRangeDeltaRenderer<R> implements DeltaRenderer<R> {

    @Override
    public R render(Delta delta) {
        return render(delta, delta.getAttributes(), delta.getEmbedded());
    }

    @SuppressWarnings("unchecked")
    protected <RANGE extends Range> RANGE copyRangeDependingOnAttributePresence(int start, int end, RANGE sourceRange) {
        if (sourceRange instanceof AttributeRange) {
            return (RANGE) new AttributeRange(Range.between(start, end), ((AttributeRange) sourceRange).getAttribute());
        } else {
            return (RANGE) Range.between(start, end);
        }
    }

    protected <RANGE extends Range> RANGE resolveParagraphRange(String text, RANGE range) {
        int start = text.substring(0, range.getStart()).lastIndexOf('\n');
        return start != -1
            ? copyRangeDependingOnAttributePresence(start + 1, range.getStart(), range)
            : copyRangeDependingOnAttributePresence(0, range.getStart(), range);
    }

    protected List<AttributeRange> findParagraphRanges(String text, AttributeRanges attributeRanges) {
        return attributeRanges.stream()
            .filter(attributeRange -> Op.NL.equals(text.substring(attributeRange.getStart(), attributeRange.getEnd())))
            .sorted()
            .map(attributeRange -> resolveParagraphRange(text, attributeRange))
            .collect(Collectors.toList());
    }

    protected Map<Range, Set<Attribute>> collectRangeAttributeMap(List<AttributeRange> paragraphRanges) {
        Map<Range, Set<Attribute>> rangeAttributeMap = new HashMap<>();
        paragraphRanges.forEach(attributeRange -> {
            Set<Attribute> attributes = rangeAttributeMap.computeIfAbsent(attributeRange.asRange(), range -> new HashSet<>());
            attributes.add(attributeRange.getAttribute());
        });
        return rangeAttributeMap;
    }

    protected String getTagByBlockTypeAttribute(Attribute attribute) {
        String tag;
        if (attribute.getType() == AttributeType.LIST) {
            tag = ListType.of(attribute.getString()) == ListType.BULLET ? "ul" : "ol";
        } else if (attribute.getType() == AttributeType.CODE_BLOCK) {
            tag = "pre";
        } else {
            throw new DeltaRendererException("Unsupported block attribute type: " + attribute.getType());
        }
        return tag;
    }

    protected Map<String, Set<Range>> collectBlocks(List<AttributeRange> paragraphRanges) {
        List<AttributeRange> blockTypeParagraphRanges = paragraphRanges.stream()
            .filter(attributeRange -> attributeRange.getAttribute().getType() == AttributeType.LIST ||
                attributeRange.getAttribute().getType() == AttributeType.CODE_BLOCK)
            .collect(Collectors.toList());

        Map<String, Set<Range>> listTypeBlockRanges = new HashMap<>();
        for (AttributeRange currentRange : blockTypeParagraphRanges) {
            String tag = getTagByBlockTypeAttribute(currentRange.getAttribute());
            Set<Range> blockRangesByTag = listTypeBlockRanges.computeIfAbsent(tag, lt -> new HashSet<>());

            Range newRange = null;
            Iterator<Range> blockRangeIterator = blockRangesByTag.iterator();
            while (blockRangeIterator.hasNext()) {
                Range blockRange = blockRangeIterator.next();
                if (blockRange.getEnd() + 1 == currentRange.getStart()) {
                    newRange = Range.between(blockRange.getStart(), currentRange.getEnd());
                    blockRangeIterator.remove();
                    break;
                }
            }

            blockRangesByTag.add(newRange != null ? newRange : currentRange.asRange());
        }
        return listTypeBlockRanges;
    }

    protected int shiftedIndex(int offset, Map<Integer, List<Integer>> shiftMap) {
        return offset + shiftMap.entrySet().stream()
            .filter(shift -> shift.getKey() <= offset)
            .flatMap(shift -> shift.getValue().stream())
            .mapToInt(value -> value)
            .sum();
    }

    protected Range renderedRange(Range range, Map<Integer, List<Integer>> shiftMap) {
        int start = shiftedIndex(range.getStart(), shiftMap);
        int end = shiftedIndex(range.getEnd(), shiftMap);
        return Range.between(start, end);
    }

    protected void insert(StringBuilder renderedText, Range attributeRange, Map<Integer, List<Integer>> shiftMap, String before, String after) {
        Range renderedRange = renderedRange(attributeRange, shiftMap);
        renderedText.insert(renderedRange.getEnd(), after);
        renderedText.insert(renderedRange.getStart(), before);
        shiftMap.computeIfAbsent(attributeRange.getStart(), integer -> new ArrayList<>()).add(before.length());
        shiftMap.computeIfAbsent(attributeRange.getEnd(), integer -> new ArrayList<>()).add(after.length());
    }

    public abstract R render(Delta delta, AttributeRanges attributeRanges, EmbedRanges embedRanges);

}
