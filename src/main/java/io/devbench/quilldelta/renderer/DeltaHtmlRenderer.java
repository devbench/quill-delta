/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.renderer;

import io.devbench.quilldelta.*;
import io.devbench.quilldelta.embed.Embed;
import io.devbench.quilldelta.embed.Image;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.*;

public class DeltaHtmlRenderer extends AbstractRangeDeltaRenderer<Document> {

    private void wrap(String tagName, StringBuilder renderedText,
                      Range attributeRange, Map<Integer, List<Integer>> shiftMap) {
        wrap(tagName, null, renderedText, attributeRange, shiftMap);
    }

    private void wrap(String tagName, String tagAttributes, StringBuilder renderedText,
                      Range attributeRange, Map<Integer, List<Integer>> shiftMap) {
        String attr = tagAttributes == null ? "" : " " + tagAttributes.trim();
        String startTag = "<" + tagName + attr + ">";
        String endTag = "</" + tagName + ">";
        insert(renderedText, attributeRange, shiftMap, startTag, endTag);
    }

    private void replaceEmbedPlaceholder(String tagName, String tagAttributes, StringBuilder renderedText,
                                         Range attributeRange, Map<Integer, List<Integer>> shiftMap) {
        String attr = tagAttributes == null ? "" : " " + tagAttributes.trim();
        String tag = "<" + tagName + attr + ">";
        Range renderedRange = renderedRange(attributeRange, shiftMap);
        renderedText.replace(renderedRange.getStart(), renderedRange.getStart() + 1, tag);
        shiftMap.computeIfAbsent(attributeRange.getStart(), integer -> new ArrayList<>()).add(tag.length() - 1);
    }

    private void renderInlineFormat(StringBuilder renderedText, Map<Integer, List<Integer>> shiftMap, Range range, Set<Attribute> attributes) {
        Set<String> tags = new HashSet<>();
        Set<String> styles = new HashSet<>();
        Set<String> classes = new HashSet<>();

        for (Attribute attribute : attributes) {
            AttributeType attributeType = attribute.getType();
            if (attributeType == AttributeType.BOLD) {
                tags.add("strong");
            } else if (attributeType == AttributeType.ITALIC) {
                tags.add("em");
            } else if (attributeType == AttributeType.UNDERLINE) {
                tags.add("u");
            } else if (attributeType == AttributeType.STRIKE) {
                tags.add("s");
            } else if (attributeType == AttributeType.SCRIPT) {
                ScriptType scriptType = ScriptType.of(attribute.getString());
                if (scriptType == ScriptType.SUB) {
                    tags.add("sub");
                } else if (scriptType == ScriptType.SUPER) {
                    tags.add("sup");
                }
            } else if (attributeType == AttributeType.COLOR) {
                styles.add("color: " + attribute.getString());
            } else if (attributeType == AttributeType.BACKGROUND) {
                styles.add("background-color: " + attribute.getString());
            } else if (attributeType == AttributeType.FONT) {
                FontType fontType = FontType.of(attribute.getString());
                classes.add("ql-font-" + fontType.getJsonName());
            } else if (attributeType == AttributeType.SIZE) {
                SizeType sizeType = SizeType.of(attribute.getString());
                classes.add("ql-size-" + sizeType.getJsonName());
            } else if (attributeType == AttributeType.LINK) {
                wrap("a", "href=\"" + attribute.getString() + "\"", renderedText, range, shiftMap);
            }
        }

        if (styles.isEmpty() && classes.isEmpty()) {
            tags.forEach(tag -> wrap(tag, renderedText, range, shiftMap));
        } else {
            if (tags.isEmpty()) {
                tags.add("span");
            }
            Iterator<String> tagIterator = tags.iterator();
            while (tagIterator.hasNext()) {
                String tag = tagIterator.next();
                String attributeString = null;
                if (!tagIterator.hasNext()) {
                    attributeString = !classes.isEmpty() ? "class=\"" + String.join(" ", classes) + "\"" : "";
                    if (!styles.isEmpty()) {
                        String stylesString = "style=\"" + String.join("; ", styles) + "\"";
                        attributeString = attributeString + (attributeString.isEmpty() ? stylesString : " " + stylesString);
                    }
                }
                wrap(tag, attributeString, renderedText, range, shiftMap);
            }
        }
    }

    private void renderBlockFormat(String text, StringBuilder renderedText, Map<Integer, List<Integer>> shiftMap, AttributeRanges attributeRanges) {
        List<AttributeRange> paragraphRanges = findParagraphRanges(text, attributeRanges);
        Map<Range, Set<Attribute>> rangeAttributeMap = collectRangeAttributeMap(paragraphRanges);

        Map<String, Set<Range>> listTypeBlockRanges = collectBlocks(paragraphRanges);
        for (String tag : listTypeBlockRanges.keySet()) {
            for (Range range : listTypeBlockRanges.get(tag)) {
                String attributes = tag.equals("pre") ? "class=\"ql-syntax\" spellcheck=\"false\"" : null;
                wrap(tag, attributes, renderedText, range, shiftMap);
            }
        }

        rangeAttributeMap.forEach((range, attributes) -> {
            String tag = "p";
            List<String> classNames = new ArrayList<>();
            for (Attribute attribute : attributes) {
                if (attribute.getType() == AttributeType.LIST) {
                    tag = "li";
                }
                if (attribute.getType() == AttributeType.ALIGN) {
                    classNames.add("ql-align-" + attribute.getString());
                }
                if (attribute.getType() == AttributeType.HEADER) {
                    tag = "h" + attribute.getNumber();
                }
                if (attribute.getType() == AttributeType.INDENT) {
                    classNames.add("ql-indent-" + attribute.getNumber());
                }
                if (attribute.getType() == AttributeType.BLOCKQUOTE) {
                    tag = "blockquote";
                }
                if (attribute.getType() == AttributeType.CODE_BLOCK) {
                    tag = "pre";
                }
            }
            if (!tag.equals("pre")) {
                String attribute = classNames.isEmpty() ? null : "class=\"" + String.join(" ", classNames) + "\"";
                wrap(tag, attribute, renderedText, range, shiftMap);
            }
        });

    }

    private void renderInlineEmbedded(StringBuilder renderedText, Map<Integer, List<Integer>> shiftMap, int embeddedOffset, Embed embed) {
        if (embed instanceof Image) {
            Image image = (Image) embed;
            replaceEmbedPlaceholder("img", "src=\"" + image.getValue() + "\"", renderedText,
                Range.between(embeddedOffset, embeddedOffset + Op.EMBED_TEXT_PLACEHOLDER.length()), shiftMap);
        }
    }

    private void wrapNonformattedNewLinesIntoParagraph(AttributeRanges attributeRanges, String text,
                                                       StringBuilder renderedText,
                                                       Map<Integer, List<Integer>> shiftMap) {
        int offset = 0;
        int nlIndex = text.indexOf('\n', offset);
        while (nlIndex != -1) {
            Range nlRange = Range.between(nlIndex, nlIndex + 1);
            if (attributeRanges.stream().map(AttributeRange::asRange).noneMatch(nlRange::equals)) {
                Range paragraphParagraph = resolveParagraphRange(text, nlRange);
                if (paragraphParagraph.length() != 0 || paragraphParagraph.getStart() != text.length() - 1) {
                    wrap("p", renderedText, paragraphParagraph, shiftMap);
                }
            }
            offset = nlIndex + 1;
            nlIndex = text.indexOf('\n', offset);
        }
    }

    @Override
    public Document render(Delta delta, AttributeRanges attributeRanges, EmbedRanges embedRanges) {
        String text = delta.getText();
        StringBuilder renderedText = new StringBuilder(text);
        Map<Integer, List<Integer>> shiftMap = new HashMap<>();
        Map<Range, Set<Attribute>> rangeAttributes = new HashMap<>();
        attributeRanges
            .forEach(attributeRange -> rangeAttributes
                .computeIfAbsent(attributeRange.asRange(), range -> new HashSet<>())
                .add(attributeRange.getAttribute()));

        rangeAttributes.keySet().forEach(range -> renderInlineFormat(renderedText, shiftMap, range, rangeAttributes.get(range)));
        embedRanges.forEach((embeddedOffset, embed) -> renderInlineEmbedded(renderedText, shiftMap, embeddedOffset, embed));
        renderBlockFormat(text, renderedText, shiftMap, attributeRanges);
        wrapNonformattedNewLinesIntoParagraph(attributeRanges, text, renderedText, shiftMap);

        return Jsoup.parse(renderedText.toString());
    }

}
