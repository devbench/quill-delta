/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.renderer;

import io.devbench.quilldelta.*;
import io.devbench.quilldelta.embed.Embed;
import io.devbench.quilldelta.embed.Image;

import java.util.*;

import static io.devbench.quilldelta.DeltaUtils.*;

public class DeltaMarkdownRenderer extends AbstractRangeDeltaRenderer<String> {

    private void wrap(String wrapChar, StringBuilder renderedText,
                      Range attributeRange, Map<Integer, List<Integer>> shiftMap) {
        wrap(wrapChar, null, renderedText, attributeRange, shiftMap);
    }

    private void wrap(String wrapChar, String afterText, StringBuilder renderedText,
                      Range attributeRange, Map<Integer, List<Integer>> shiftMap) {
        String before;
        String after;

        if (afterText != null) {
            before = wrapChar;
            after = afterText;
        } else {
            before = wrapChar;
            after = wrapChar;
        }
        insert(renderedText, attributeRange, shiftMap, before, after);
    }

    private void replaceEmbedPlaceholder(String replaceText, StringBuilder renderedText,
                                         Range attributeRange, Map<Integer, List<Integer>> shiftMap) {
        Range renderedRange = renderedRange(attributeRange, shiftMap);
        renderedText.replace(renderedRange.getStart(), renderedRange.getStart() + 1, replaceText);
        shiftMap.computeIfAbsent(attributeRange.getStart(), integer -> new ArrayList<>()).add(replaceText.length() - 1);
    }

    private void renderInlineFormat(StringBuilder renderedText, Map<Integer, List<Integer>> shiftMap, AttributeRange attributeRange) {
        AttributeType attributeType = attributeRange.getAttribute().getType();
        if (attributeType == AttributeType.BOLD) {
            wrap("**", renderedText, attributeRange.asRange(), shiftMap);
        } else if (attributeType == AttributeType.ITALIC) {
            wrap("*", renderedText, attributeRange.asRange(), shiftMap);
        } else if (attributeType == AttributeType.LINK) {
            String href = attributeRange.getAttribute().getString();
            wrap("[", "](" + href + ")", renderedText, attributeRange.asRange(), shiftMap);
        }
    }

    private void renderBlockFormat(String text, StringBuilder renderedText, Map<Integer, List<Integer>> shiftMap, AttributeRanges attributeRanges) {
        List<AttributeRange> paragraphRanges = findParagraphRanges(text, attributeRanges);
        Map<Range, Set<Attribute>> rangeAttributeMap = collectRangeAttributeMap(paragraphRanges);

        Set<Range> renderedPreRanges = new HashSet<>();
        Map<String, Set<Range>> listTypeBlockRanges = collectBlocks(paragraphRanges);
        for (String tag : listTypeBlockRanges.keySet()) {
            for (Range range : listTypeBlockRanges.get(tag)) {
                if (tag.equals("pre")) {
                    wrap("```\n", "\n```", renderedText, range, shiftMap);
                    renderedPreRanges.add(range);
                }
            }
        }

        rangeAttributeMap.forEach((range, attributes) -> {
            String tag = "p";
            String indent = "";
            for (Attribute attribute : attributes) {
                if (attribute.getType() == AttributeType.INDENT) {
                    indent = fill("  ", attribute.getNumber());
                }
            }
            for (Attribute attribute : attributes) {
                if (attribute.getType() == AttributeType.LIST) {
                    tag = "li";
                    wrap(indent + "- ", "", renderedText, range, shiftMap);
                }
                if (attribute.getType() == AttributeType.HEADER) {
                    tag = "h" + attribute.getNumber();
                    wrap(fill("#", attribute.getNumber()) + " ", "", renderedText, range, shiftMap);
                }
                if (attribute.getType() == AttributeType.BLOCKQUOTE) {
                    tag = "blockquote";
                    wrap(" > ", "", renderedText, range, shiftMap);
                }
                if (attribute.getType() == AttributeType.CODE_BLOCK) {
                    tag = "pre";
                    if (renderedPreRanges.stream().noneMatch(renderedPreRange -> renderedPreRange.intersects(range))) {
                        wrap("```\n", "\n```", renderedText, range, shiftMap);
                    }
                }
            }

            if (tag.equals("p")) {
                wrap(indent, "", renderedText, range, shiftMap);
            }
        });

    }

    private void renderInlineEmbedded(StringBuilder renderedText, Map<Integer, List<Integer>> shiftMap,
                                      int embeddedOffset, Embed embed, AttributeRanges attributeRanges) {
        if (embed instanceof Image) {
            String altText = attributeRanges.filterByAttributeType(AttributeType.ALT)
                .stream()
                .findFirst()
                .map(AttributeRange::getAttribute)
                .map(Attribute::getString)
                .orElse("image");
            Image image = (Image) embed;
            replaceEmbedPlaceholder("![" + altText + "](" + image.getValue() + ")", renderedText,
                Range.between(embeddedOffset, embeddedOffset + Op.EMBED_TEXT_PLACEHOLDER.length()), shiftMap);
        }
    }

    private void replaceNonformattedNewLinesWithDoubleSpaceAtEndOfLine(AttributeRanges attributeRanges, String text,
                                                                       StringBuilder renderedText,
                                                                       Map<Integer, List<Integer>> shiftMap) {
        int offset = 0;
        int lastNlIndex = text.length() - 1;
        int nlIndex = text.indexOf('\n', offset);
        while (nlIndex != -1) {
            if (nlIndex != lastNlIndex) {
                Range nlRange = Range.between(nlIndex, nlIndex + 1);
                if (attributeRanges.stream().map(AttributeRange::asRange).noneMatch(nlRange::equals)) {
                    wrap("  ", "", renderedText, nlRange, shiftMap);
                }
            }
            offset = nlIndex + 1;
            nlIndex = text.indexOf('\n', offset);
        }
    }

    @Override
    public String render(Delta delta, AttributeRanges attributeRanges, EmbedRanges embedRanges) {

        String text = delta.getText();

        StringBuilder renderedText = new StringBuilder(text);
        Map<Integer, List<Integer>> shiftMap = new HashMap<>();
        attributeRanges.forEach(attributeRange -> renderInlineFormat(renderedText, shiftMap, attributeRange));
        embedRanges.forEach((embeddedOffset, embed) -> {
            AttributeRanges embedAttributeRanges = attributeRanges.inRange(Range.between(embeddedOffset, embeddedOffset + Op.EMBED_TEXT_PLACEHOLDER.length()));
            renderInlineEmbedded(renderedText, shiftMap, embeddedOffset, embed, embedAttributeRanges);
        });
        renderBlockFormat(text, renderedText, shiftMap, attributeRanges);
        replaceNonformattedNewLinesWithDoubleSpaceAtEndOfLine(attributeRanges, text, renderedText, shiftMap);

        return renderedText.toString();
    }

}
