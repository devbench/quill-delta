/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.parser;

import io.devbench.quilldelta.*;

import static io.devbench.quilldelta.DeltaUtils.*;

public class DeltaMarkdownParser implements DeltaParser {

    @Override
    public Delta parse(String source) {
        Delta delta = new Delta(source);

        processDoubleSpaceLineEndings(delta);
        processWrappedToAttribute(delta, "***", Attribute.create(AttributeType.BOLD, true), Attribute.create(AttributeType.ITALIC, true));
        processWrappedToAttribute(delta, "**", Attribute.create(AttributeType.BOLD, true));
        processWrappedToAttribute(delta, "*", Attribute.create(AttributeType.ITALIC, true));
        processPrefixedToAttribute(delta, "> ", Attribute.create(AttributeType.BLOCKQUOTE, true));
        processPrefixedToAttribute(delta, " > ", Attribute.create(AttributeType.BLOCKQUOTE, true));
        processPrefixedToAttribute(delta, "- ", Attribute.create(AttributeType.LIST, ListType.BULLET.getJsonName()));
        for (int i = 1; i <= 6; i++) {
            processPrefixedToAttribute(delta, fill("#", i) + " ",
                Attribute.create(AttributeType.HEADER, i));
            processPrefixedToAttribute(delta, fill("  ", i) + "- ",
                Attribute.create(AttributeType.LIST, ListType.BULLET.getJsonName()),
                Attribute.create(AttributeType.INDENT, i));
        }
        processBlockToAttribute(delta, "```", Attribute.create(AttributeType.CODE_BLOCK, true));
        processLinks(delta);

        return delta;
    }

    private void walkLinesWithProtection(Delta delta, LineConsumer lineConsumer) {
        boolean modified = true;
        while (modified) {
            boolean inBlock = false;
            int offset = 0;
            for (String line : delta.getText().split("\n")) {
                if (line.trim().startsWith("```")) {
                    inBlock = !inBlock;
                }
                modified = lineConsumer.accept(line, offset, inBlock);
                if (modified) {
                    break;
                }
                offset = offset + line.length() + 1;
            }
        }
    }

    private void processBlockToAttribute(Delta delta, String blockMarker, Attribute... attributes) {
        boolean modified = true;
        int blockStart = -1;
        while (modified) {
            modified = false;
            int offset = 0;
            for (String line : delta.getText().split("\n")) {
                if (line.trim().startsWith(blockMarker)) {
                    if (blockStart == -1) {
                        blockStart = offset;
                        int deleteLen = line.length() + 1;
                        delta.apply(Ops.from(
                            Op.retain(offset),
                            Op.delete(deleteLen)));
                    } else {
                        int deleteLen = line.length() + 1;
                        delta.apply(Ops.from(
                            Op.retain(offset),
                            Op.delete(deleteLen)));
                        String block = delta.getText().substring(blockStart, offset);
                        int nlIndexOffset = 0;
                        int nlIndex;
                        while ((nlIndex = block.indexOf("\n", nlIndexOffset)) != -1) {
                            delta.apply(Ops.from(
                                Op.retain(blockStart + nlIndex),
                                Op.retain(1, attributes)));
                            nlIndexOffset = nlIndex + 1;
                        }
                    }
                    modified = true;
                    break;
                }
                offset = offset + line.length() + 1;
            }
        }
    }

    private void processDoubleSpaceLineEndings(Delta delta) {
        int index;
        while ((index = delta.getText().indexOf("  \n")) != -1) {
            delta.apply(Ops.from(Op.retain(index), Op.delete(2)));
        }
    }

    private void processLinks(Delta delta) {
        walkLinesWithProtection(delta, (line, offset, inBlock) -> {
            int startIndex = line.indexOf("[");
            if (startIndex != -1) {
                int middleIndex = line.indexOf("](", startIndex + 1);
                if (middleIndex != -1) {
                    int endIndex = line.indexOf(")", middleIndex + 1);
                    if (endIndex != -1) {
                        String linkText = line.substring(startIndex + 1, middleIndex);
                        String linkUrl = line.substring(middleIndex + 2, endIndex);
                        int deleteLen = (endIndex - startIndex) + 1;
                        delta.apply(Ops.from(
                            Op.retain(offset + startIndex),
                            Op.delete(deleteLen)));
                        delta.apply(Ops.from(
                            Op.retain(offset + startIndex),
                            Op.insert(linkText, Attribute.create(AttributeType.LINK, linkUrl))));
                        return true;
                    }
                }
            }
            return false;
        });
    }

    private void processPrefixedToAttribute(Delta delta, String prefix, Attribute... attributes) {
        int prefixLen = prefix.length();
        walkLinesWithProtection(delta, (line, offset, inBlock) -> {
            if (line.startsWith(prefix)) {
                delta.apply(Ops.from(
                    Op.retain(offset),
                    Op.delete(prefixLen)));
                int eolIndex = offset + line.length() - prefixLen;
                delta.apply(Ops.from(
                    Op.retain(eolIndex),
                    Op.retain(1, attributes)
                ));
                return true;

            }
            return false;
        });
    }

    private void processWrappedToAttribute(Delta delta, String wrap, Attribute... attributes) {
        int wrapLen = wrap.length();
        walkLinesWithProtection(delta, (line, offset, inBlock) -> {
            if (inBlock) {
                return false;
            }
            int startIndex = line.indexOf(wrap);
            if (startIndex != -1) {
                startIndex = offset + startIndex;
                int closeIndex = line.indexOf(wrap, (startIndex - offset) + wrapLen);
                if (closeIndex != -1) {
                    closeIndex = offset + closeIndex;
                    delta.apply(Ops.from(
                        Op.retain(closeIndex),
                        Op.delete(wrapLen)));
                    delta.apply(Ops.from(
                        Op.retain(startIndex),
                        Op.delete(wrapLen)
                    ));
                    delta.apply(Ops.from(
                        Op.retain(startIndex),
                        Op.retain(closeIndex - startIndex - wrapLen, attributes)
                    ));
                    return true;
                }
            }
            return false;
        });
    }

    @FunctionalInterface
    interface LineConsumer {
        boolean accept(String line, int offset, boolean inBlock);
    }
}
