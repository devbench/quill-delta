/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.parser;

import io.devbench.quilldelta.*;
import org.jetbrains.annotations.Nullable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeltaHtmlParser implements DeltaParser {

    private static final Pattern QL_INDENT_NUMBER_PATTERN = Pattern.compile(".*ql-indent-([0-9]+).*");
    private static final Pattern QL_FONT_NAME_PATTERN = Pattern.compile(".*ql-font-([a-z0-9]+).*");
    private static final Pattern QL_ALIGN_NAME_PATTERN = Pattern.compile(".*ql-align-([a-z0-9]+).*");
    private static final Pattern QL_SIZE_NAME_PATTERN = Pattern.compile(".*ql-size-([a-z0-9]+).*");

    @Override
    public Delta parse(String source) {
        Delta delta = new Delta(source);

        Document document = Jsoup.parseBodyFragment(delta.getText());
        Element body = document.body();


        Ops ops = new Ops();
        walk(body, ops, new AtomicReference<>(s -> s));

        return new Delta(ops);
    }

    private void walk(Node node, Ops ops, AtomicReference<Function<String, String>> nextTextNodeFunctionReference) {
        if (node instanceof TextNode) {
            TextNode textNode = (TextNode) node;
            parseBlock(textNode, ops, nextTextNodeFunctionReference);
        } else {
            node.childNodes().forEach(childNode -> walk(childNode, ops, nextTextNodeFunctionReference));
        }
    }

    private boolean hasParentTag(Node node, String tagName) {
        return findParentTag(node, tagName).isPresent();
    }

    private Optional<Element> findParentTag(Node node, @Nullable String tagName) {
        Node currentNode = node;
        while (currentNode.hasParent()) {
            currentNode = currentNode.parentNode();
            if (currentNode instanceof Element
                && (tagName == null || tagName.equals(((Element) currentNode).tagName()))) {
                return Optional.of((Element) currentNode);
            }
        }
        return Optional.empty();
    }

    private boolean hasDirectParentTag(Node node, String tagName) {
        return node.hasParent()
            && node.parentNode() instanceof Element
            && ((Element) node.parentNode()).tagName().equals(tagName);
    }

    private void parseInline(TextNode textNode, Ops ops, AtomicReference<Function<String, String>> nextTextNodeFunctionReference) {
        String text = nextTextNodeFunctionReference.get().apply(textNode.getWholeText());

        List<Attribute> attributes = new ArrayList<>();
        if (hasParentTag(textNode, "strong")) {
            attributes.add(Attribute.create(AttributeType.BOLD, true));
        }
        if (hasParentTag(textNode, "em")) {
            attributes.add(Attribute.create(AttributeType.ITALIC, true));
        }
        if (hasParentTag(textNode, "u")) {
            attributes.add(Attribute.create(AttributeType.UNDERLINE, true));
        }
        if (hasParentTag(textNode, "s")) {
            attributes.add(Attribute.create(AttributeType.STRIKE, true));
        }
        if (hasParentTag(textNode, "sub")) {
            attributes.add(Attribute.create(AttributeType.SCRIPT, ScriptType.SUB.getJsonName()));
        }
        if (hasParentTag(textNode, "sup")) {
            attributes.add(Attribute.create(AttributeType.SCRIPT, ScriptType.SUPER.getJsonName()));
        }
        if (hasParentTag(textNode, "a")) {
            @SuppressWarnings("OptionalGetWithoutIsPresent")
            Element anchor = findParentTag(textNode, "a").get();
            String href = anchor.attr("href");
            attributes.add(Attribute.create(AttributeType.LINK, href));
        }

        parseStyle(textNode, attributes);

        ops.add(Op.insert(text, attributes.toArray(new Attribute[0])));
    }

    private void parseStyle(TextNode textNode, List<Attribute> attributes) {
        findParentTag(textNode, null).ifPresent(element -> {
            if (element.hasAttr("style")) {
                String style = element.attr("style");
                for (String expression : style.split(";")) {
                    String trimmedExpression = expression.trim();
                    int colonIndex = trimmedExpression.indexOf(':');
                    String property = trimmedExpression.substring(0, colonIndex).trim();
                    String value = trimmedExpression.substring(colonIndex + 1).trim();

                    if (property.equals("color")) {
                        attributes.add(Attribute.create(AttributeType.COLOR, value));
                    } else if (property.equals("background-color")) {
                        attributes.add(Attribute.create(AttributeType.BACKGROUND, value));
                    }
                }
            }
            if (element.hasAttr("class")) {
                String classString = element.attr("class");
                if (classString.contains("ql-font-")) {
                    findInStringByPattern(QL_FONT_NAME_PATTERN, classString).ifPresent(fontName -> {
                        attributes.add(Attribute.create(AttributeType.FONT, fontName));
                    });
                }
                if (classString.contains("ql-size-")) {
                    findInStringByPattern(QL_SIZE_NAME_PATTERN, classString).ifPresent(sizeName -> {
                        attributes.add(Attribute.create(AttributeType.SIZE, sizeName));
                    });
                }
            }
        });
    }

    private void parseBlock(TextNode textNode, Ops ops, AtomicReference<Function<String, String>> nextTextNodeFunctionReference) {
        if (hasDirectParentTag(textNode, "pre")) {
            String text = nextTextNodeFunctionReference.get().apply(textNode.getWholeText());
            Iterator<String> lineIterator = Arrays.stream(text.split("\n")).iterator();
            while (lineIterator.hasNext()) {
                String line = lineIterator.next();
                ops.add(Op.insert(line));
                ops.add(Op.insert("\n", Attribute.create(AttributeType.CODE_BLOCK, true)));
            }
            if (!text.endsWith("\n")) {
                nextTextNodeFunctionReference.set(nextTextNodeText -> {
                    if (nextTextNodeText.equals("\n")) {
                        nextTextNodeFunctionReference.set(s -> s);
                        return "";
                    }
                    return nextTextNodeText;
                });
            }
        } else if (hasParentTag(textNode, "blockquote")) {
            @SuppressWarnings("OptionalGetWithoutIsPresent")
            Element blockquote = findParentTag(textNode, "blockquote").get();
            AlignType alignType = getElementAlignFromClass(blockquote);
            SizeType sizeType = getElementSizeFromClass(blockquote);
            int indent = getElementIndentFromClass(blockquote);
            nextTextNodeFunctionReference.set(nextTextNodeText -> {
                if (nextTextNodeText.equals("\n")) {
                    applyIndentAndAlignAwareLineFormatter(indent, alignType, sizeType, ops,
                        Attribute.create(AttributeType.BLOCKQUOTE, true));
                    nextTextNodeFunctionReference.set(s -> s);
                    return "";
                }
                return nextTextNodeText;
            });
            parseInline(textNode, ops, nextTextNodeFunctionReference);
        } else if (hasParentTag(textNode, "li")) {
            @SuppressWarnings("OptionalGetWithoutIsPresent")
            Element li = findParentTag(textNode, "li").get();
            int indent = getElementIndentFromClass(li);
            ListType listType = hasParentTag(textNode, "ul") ? ListType.BULLET : ListType.ORDERED;
            AlignType alignType = getElementAlignFromClass(li);
            SizeType sizeType = getElementSizeFromClass(li);
            nextTextNodeFunctionReference.set(nextTextNodeText -> {
                if (nextTextNodeText.equals("\n")) {
                    applyIndentAndAlignAwareLineFormatter(indent, alignType, sizeType, ops,
                        Attribute.create(AttributeType.LIST, listType.getJsonName()));
                    nextTextNodeFunctionReference.set(s -> s);
                    return "";
                }
                return nextTextNodeText;
            });
            parseInline(textNode, ops, nextTextNodeFunctionReference);
        } else {
            @SuppressWarnings("OptionalGetWithoutIsPresent")
            Element paragraph = findParentTag(textNode, null).get();
            AlignType alignType = getElementAlignFromClass(paragraph);
            SizeType sizeType = getElementSizeFromClass(paragraph);
            int indent = getElementIndentFromClass(paragraph);
            if (alignType != AlignType.LEFT || indent > 0) {
                nextTextNodeFunctionReference.set(nextTextNodeText -> {
                    if (nextTextNodeText.equals("\n")) {
                        applyIndentAndAlignAwareLineFormatter(indent, alignType, sizeType, ops);
                        nextTextNodeFunctionReference.set(s -> s);
                        return "";
                    }
                    return nextTextNodeText;
                });
            }
            parseInline(textNode, ops, nextTextNodeFunctionReference);
        }
    }

    private void applyIndentAndAlignAwareLineFormatter(int indent, AlignType alignType, SizeType sizeType, Ops ops, Attribute... additionalAttributes) {
        List<Attribute> attributes = new ArrayList<>();
        if (indent > 0) {
            attributes.add(Attribute.create(AttributeType.INDENT, indent));
        }
        if (alignType != AlignType.LEFT) {
            attributes.add(Attribute.create(AttributeType.ALIGN, alignType.getJsonName()));
        }
        if (sizeType != SizeType.NORMAL) {
            attributes.add(Attribute.create(AttributeType.SIZE, sizeType.getJsonName()));
        }
        attributes.addAll(Arrays.asList(additionalAttributes));
        ops.add(Op.insert("\n", attributes.toArray(new Attribute[0])));
    }

    private int getElementIndentFromClass(Element element) {
        if (element.hasAttr("class")) {
            String classString = element.attr("class");
            if (classString.contains("ql-indent-")) {
                return findInStringByPattern(QL_INDENT_NUMBER_PATTERN, classString)
                    .map(Integer::parseInt)
                    .orElse(0);
            }
        }
        return 0;
    }

    private AlignType getElementAlignFromClass(Element element) {
        if (element.hasAttr("class")) {
            String classString = element.attr("class");
            if (classString.contains("ql-align-")) {
                return findInStringByPattern(QL_ALIGN_NAME_PATTERN, classString)
                    .map(AlignType::of)
                    .orElse(AlignType.LEFT);
            }
        }
        return AlignType.LEFT;
    }

    private SizeType getElementSizeFromClass(Element element) {
        if (element.hasAttr("class")) {
            String classString = element.attr("class");
            if (classString.contains("ql-size-")) {
                return findInStringByPattern(QL_SIZE_NAME_PATTERN, classString)
                    .map(SizeType::of)
                    .orElse(SizeType.NORMAL);
            }
        }
        return SizeType.NORMAL;
    }

    private Optional<String> findInStringByPattern(Pattern pattern, String string) {
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            String value = matcher.group(1);
            if (value != null && !value.isEmpty()) {
                return Optional.of(value);
            }
        }
        return Optional.empty();
    }

}
