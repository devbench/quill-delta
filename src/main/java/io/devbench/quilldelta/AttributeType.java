/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

public enum AttributeType {

    BOLD, ITALIC, UNDERLINE, STRIKE, BLOCKQUOTE, CODE_BLOCK,
    HEADER(AttributeValueType.NUMBER),
    INDENT(AttributeValueType.NUMBER),
    FONT(AttributeValueType.STRING),
    SIZE(AttributeValueType.STRING),
    COLOR(AttributeValueType.STRING),
    BACKGROUND(AttributeValueType.STRING),
    ALIGN(AttributeValueType.STRING),
    LIST(AttributeValueType.STRING),
    SCRIPT(AttributeValueType.STRING),
    LINK(AttributeValueType.STRING),
    ALT(AttributeValueType.STRING);

    private final AttributeValueType valueType;

    AttributeType() {
        valueType = AttributeValueType.BOOL;
    }

    AttributeType(AttributeValueType type) {
        valueType = type;
    }

    public AttributeValueType valueType() {
        return valueType;
    }

    public String getJsonName() {
        return name().toLowerCase().replace("_", "-");
    }

    public static AttributeType from(String jsonName) {
        for (AttributeType value : AttributeType.values()) {
            if (value.getJsonName().equals(jsonName)) {
                return value;
            }
        }
        return null;
    }
}
