/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import elemental.json.JsonNull;
import elemental.json.JsonValue;
import io.devbench.quilldelta.exception.DeltaOpException;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import java.io.Serializable;

@Getter
@Builder
@EqualsAndHashCode
@ToString
public class Attribute implements Serializable {

    private final AttributeType type;
    private final Boolean bool;
    private final String string;
    private final Integer number;

    public static Attribute from(String typeName, JsonValue jsonValue) {
        AttributeType type = AttributeType.from(typeName);
        if (type == null) {
            throw new DeltaOpException("Invalid attribute type: " + typeName);
        }
        AttributeBuilder builder = Attribute.builder().type(type);
        if (type.valueType() == AttributeValueType.STRING) {
            builder.string(jsonValue instanceof JsonNull ? null : jsonValue.asString());
        } else if (type.valueType() == AttributeValueType.NUMBER) {
            builder.number(jsonValue instanceof JsonNull ? null : (int) jsonValue.asNumber());
        } else if (type.valueType() == AttributeValueType.BOOL) {
            builder.bool(jsonValue instanceof JsonNull ? null : jsonValue.asBoolean());
        }
        return builder.build();
    }

    public static Attribute create(AttributeType type, Object value) {
        AttributeBuilder builder = Attribute.builder().type(type);
        if (value != null) {
            if (type.valueType() == AttributeValueType.STRING) {
                builder.string((String) value);
            } else if (type.valueType() == AttributeValueType.NUMBER) {
                builder.number((int) value);
            } else if (type.valueType() == AttributeValueType.BOOL) {
                builder.bool((boolean) value);
            }
        }
        return builder.build();
    }

    public boolean isDelete() {
        return (type.valueType() == AttributeValueType.BOOL && bool == null) ||
            (type.valueType() == AttributeValueType.STRING && string == null) ||
            (type.valueType() == AttributeValueType.NUMBER && number == null);
    }

    public boolean isValue() {
        return !isDelete();
    }

}
