/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import lombok.experimental.Delegate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AttributeRanges implements Set<AttributeRange> {

    @Delegate
    private final Set<AttributeRange> attributeRangeSet;

    public AttributeRanges() {
        attributeRangeSet = new HashSet<>();
    }

    public AttributeRanges(Collection<? extends AttributeRange> collection) {
        attributeRangeSet = new HashSet<>(collection);
    }

    public AttributeRanges inRange(Range range) {
        return new AttributeRanges(stream()
            .filter(attributeRange -> attributeRange.intersects(range))
            .collect(Collectors.toSet()));
    }

    public AttributeRanges filterByAttributeType(AttributeType attributeType) {
        return new AttributeRanges(stream()
            .filter(attributeRange -> attributeRange.getAttribute().getType() == attributeType)
            .collect(Collectors.toSet()));
    }

    public List<Integer> sectionStartPoints() {
        return Stream.concat(
            stream().map(AttributeRange::getStart),
            stream().map(AttributeRange::getEnd))
            .distinct()
            .sorted()
            .collect(Collectors.toList());
    }

    public void splitRangeAttributesByRange(Range range) {
        AttributeRanges ranges = inRange(range);
        for (AttributeRange attributeRange : ranges) {
            Range[] partitions = attributeRange.partitionWith(range);
            Attribute attribute = attributeRange.getAttribute();
            remove(attributeRange);
            for (Range partition : partitions) {
                if (!partition.isEmpty()) {
                    add(new AttributeRange(partition, attribute));
                }
            }
        }
    }

    public void mergeContiguousRanges() {
        Set<Attribute> attributes = stream()
            .map(AttributeRange::getAttribute)
            .filter(attribute -> attribute.getType() != AttributeType.LIST)
            .filter(attribute -> attribute.getType() != AttributeType.CODE_BLOCK)
            .collect(Collectors.toSet());

        for (Attribute attribute : attributes) {
            List<AttributeRange> orderedRanges = stream()
                .filter(attributeRange -> attributeRange.getAttribute().equals(attribute))
                .sorted()
                .collect(Collectors.toList());
            Iterator<AttributeRange> orderedRangeIterator = orderedRanges.iterator();
            AttributeRange previousRange = orderedRangeIterator.next();
            Set<AttributeRange> toRemove = new HashSet<>();
            Set<AttributeRange> merged = new HashSet<>();
            while (orderedRangeIterator.hasNext()) {
                AttributeRange currentRange = orderedRangeIterator.next();
                if (previousRange.intersects(currentRange) || (previousRange.getEnd() == currentRange.getStart())) {
                    toRemove.add(previousRange);
                    toRemove.add(currentRange);
                    AttributeRange mergedRange = new AttributeRange(Range.between(previousRange.getStart(), currentRange.getEnd()), attribute);
                    merged.add(mergedRange); // add new merged range
                    merged.remove(previousRange); // remove previous if it was a merged range
                    previousRange = mergedRange;
                } else {
                    previousRange = currentRange;
                }
            }

            removeAll(toRemove);
            addAll(merged);
        }
    }

    public void shift(int offset, int length) {
        if (length < 0) {
            Range rangeToRemove = Range.between(offset + length, offset);
            splitRangeAttributesByRange(rangeToRemove);
            removeIf(attributeRange -> attributeRange.isSubsetOf(rangeToRemove));
        } else {
            Set<AttributeRange> rangesToResize = new HashSet<>();
            stream()
                .filter(attributeRange -> attributeRange.getStart() < offset)
                .filter(attributeRange -> attributeRange.getEnd() > offset)
                .forEach(rangesToResize::add);
            removeAll(rangesToResize);
            rangesToResize.forEach(attributeRange -> add(new AttributeRange(
                Range.between(attributeRange.getStart(), attributeRange.getEnd() + length), attributeRange.getAttribute())));
        }

        Set<AttributeRange> rangesToShift = new HashSet<>();
        stream().filter(attributeRange -> attributeRange.getStart() >= offset).forEach(rangesToShift::add);
        removeAll(rangesToShift);
        rangesToShift.forEach(attributeRange -> add(new AttributeRange(attributeRange.offsetBy(length), attributeRange.getAttribute())));
    }
}
