/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import elemental.json.JsonObject;
import io.devbench.quilldelta.embed.CommonEmbedFactory;
import io.devbench.quilldelta.embed.Embed;
import io.devbench.quilldelta.embed.EmbedFactory;
import org.jetbrains.annotations.NotNull;
import java.util.HashSet;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.function.Predicate;

public final class EmbedManager {

    private static final EmbedManager INSTANCE = new EmbedManager();

    private final Set<EmbedFactory<?>> embedFactories;
    private final CommonEmbedFactory commonEmbedFactory;

    public static EmbedManager getInstance() {
        return INSTANCE;
    }

    private EmbedManager() {
        commonEmbedFactory = new CommonEmbedFactory();
        embedFactories = new HashSet<>();
        @SuppressWarnings("rawtypes")
        ServiceLoader<EmbedFactory> loader = ServiceLoader.load(EmbedFactory.class);
        for (EmbedFactory<?> embedFactory : loader) {
            embedFactories.add(embedFactory);
        }
    }

    @NotNull
    public <E extends Embed> E createEmbed(@NotNull JsonObject jsonObject) {
        EmbedFactory<E> embedFactory = findEmbedFactory(factory -> factory.isApplicable(jsonObject));
        return embedFactory.createEmbed(jsonObject);
    }

    @NotNull
    public <E extends Embed> JsonObject createJson(@NotNull E embed) {
        Class<? extends Embed> embedClass = embed.getClass();
        EmbedFactory<E> embedFactory = findEmbedFactory(factory -> factory.getEmbedType().equals(embedClass));
        return embedFactory.createJson(embed);
    }

    private <E extends Embed> EmbedFactory<E> findEmbedFactory(Predicate<EmbedFactory<?>> filterPredicate) {
        @SuppressWarnings("unchecked")
        EmbedFactory<E> embedFactory = (EmbedFactory<E>) embedFactories.stream()
            .filter(filterPredicate)
            .findFirst()
            .orElse(commonEmbedFactory);
        return embedFactory;
    }

}
