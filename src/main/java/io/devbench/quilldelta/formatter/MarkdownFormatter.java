/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.formatter;

import io.devbench.quilldelta.Delta;
import io.devbench.quilldelta.parser.DeltaMarkdownParser;
import io.devbench.quilldelta.parser.DeltaParser;
import io.devbench.quilldelta.renderer.DeltaMarkdownRenderer;
import io.devbench.quilldelta.renderer.DeltaRenderer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class MarkdownFormatter implements DeltaFormatter {

    private final DeltaRenderer<?> renderer;
    private final DeltaParser parser;

    public MarkdownFormatter() {
        renderer = new DeltaMarkdownRenderer();
        parser = new DeltaMarkdownParser();
    }

    @NotNull
    @Override
    public String getFormatterName() {
        return "Markdown";
    }

    @NotNull
    @Override
    public Delta parse(@NotNull String formattedText) {
        return parser.parse(formattedText);
    }

    @NotNull
    @Override
    public String render(@NotNull Delta delta) {
        return (String) renderer.render(delta);
    }

    @Nullable
    @Override
    public DeltaRenderer<?> getRenderer() {
        return renderer;
    }

    @Nullable
    @Override
    public DeltaParser getParser() {
        return parser;
    }
}
