/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.formatter;

import io.devbench.quilldelta.Delta;
import io.devbench.quilldelta.parser.DeltaParser;
import io.devbench.quilldelta.renderer.DeltaRenderer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface DeltaFormatter {

    @NotNull
    String getFormatterName();

    @NotNull
    Delta parse(@NotNull String formattedText);

    @NotNull
    String render(@NotNull Delta delta);

    @Nullable
    DeltaRenderer<?> getRenderer();

    @Nullable
    DeltaParser getParser();
}
