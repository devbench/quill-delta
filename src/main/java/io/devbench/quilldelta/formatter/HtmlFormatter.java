/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.formatter;

import io.devbench.quilldelta.Delta;
import io.devbench.quilldelta.parser.DeltaHtmlParser;
import io.devbench.quilldelta.parser.DeltaParser;
import io.devbench.quilldelta.renderer.DeltaHtmlRenderer;
import io.devbench.quilldelta.renderer.DeltaRenderer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.nodes.Document;

public class HtmlFormatter implements DeltaFormatter {

    private final DeltaRenderer<?> renderer;
    private final DeltaParser parser;

    public HtmlFormatter() {
        renderer = new DeltaHtmlRenderer();
        parser = new DeltaHtmlParser();
    }

    @NotNull
    @Override
    public String getFormatterName() {
        return "HTML";
    }

    @NotNull
    @Override
    public Delta parse(@NotNull String formattedText) {
        return parser.parse(formattedText);
    }

    @NotNull
    @Override
    public String render(@NotNull Delta delta) {
        Document renderedDocument = (Document) renderer.render(delta);
        Document document = new Document("");
        document.outputSettings().indentAmount(0).prettyPrint(false);
        document.appendChild(renderedDocument.body().clone());
        return document.body().html();
    }

    @Nullable
    @Override
    public DeltaRenderer<?> getRenderer() {
        return renderer;
    }

    @Nullable
    @Override
    public DeltaParser getParser() {
        return parser;
    }
}
