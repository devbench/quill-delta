/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import io.devbench.quilldelta.embed.Embed;
import lombok.AccessLevel;
import lombok.Getter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Delta {

    private final StringBuilder stringBuilder;

    @Getter(AccessLevel.PACKAGE)
    private final AttributeRanges attributes;

    @Getter(AccessLevel.PACKAGE)
    private final EmbedRanges embedded;

    public Delta() {
        embedded = new EmbedRanges();
        attributes = new AttributeRanges();
        stringBuilder = new StringBuilder("\n");
    }

    public Delta(String initialText) {
        this();
        if (initialText.endsWith("\n")) {
            stringBuilder.setLength(0);
        }
        stringBuilder.insert(0, initialText);
    }

    public Delta(Op... ops) {
        this();
        applyInitialOps(Ops.from(ops));
    }

    public Delta(Ops ops) {
        this();
        applyInitialOps(ops);
    }

    private void applyInitialOps(Ops ops) {
        stringBuilder.setLength(0);
        apply(ops);
        ensureNewLineAtEndOfDocument();
    }

    private void ensureNewLineAtEndOfDocument() {
        if (!stringBuilder.toString().endsWith("\n")) {
            stringBuilder.append("\n");
        }
    }

    public String getText() {
        return stringBuilder.toString();
    }

    private String extractText(Range range) {
        return stringBuilder.substring(range.getStart(), range.getEnd());
    }

    public Ops getOps() {
        Ops ops = new Ops();

        int startPoint = 0;
        List<Integer> sectionPoints = new ArrayList<>(attributes.sectionStartPoints());
        sectionPoints.addAll(embedded.sectionStartPoints());
        Collections.sort(sectionPoints);

        for (Integer sectionPoint : sectionPoints) {
            if (startPoint < sectionPoint) {
                Range range = Range.between(startPoint, sectionPoint);
                String text = extractText(range);
                Attribute[] attributes = this.attributes.inRange(range)
                    .stream()
                    .map(AttributeRange::getAttribute)
                    .toArray(Attribute[]::new);
                Op insert = text.equals(Op.EMBED_TEXT_PLACEHOLDER) && embedded.containsKey(startPoint)
                    ? Op.embed(embedded.get(startPoint), attributes)
                    : Op.insert(text, attributes);
                ops.add(insert);
            }
            startPoint = sectionPoint;
        }

        if (startPoint < stringBuilder.length() - 1) {
            ops.add(Op.insert(extractText(Range.between(startPoint, stringBuilder.length()))));
        }

        if (ops.isEmpty()) {
            ops.add(Op.insert(stringBuilder.toString()));
        }

        return ops;
    }

    public void apply(Ops ops) {
        int offset = 0;
        for (Op op : ops) {
            offset = apply(offset, op);
        }
        attributes.mergeContiguousRanges();
    }

    public void clear() {
        embedded.clear();
        attributes.clear();
        stringBuilder.setLength(0);
        stringBuilder.append("\n");
    }

    private int apply(int offset, Op op) {
        if (op.isRetain()) {
            return processAttributes(Range.between(offset, offset + op.getRetain()), op);
        } else if (op.isInsert()) {
            String insertValue = op.getInsert();
            if (insertValue != null) {
                stringBuilder.insert(offset, insertValue);
                int length = insertValue.length();
                shift(offset, length);
                return processAttributes(Range.between(offset, offset + length), op);
            } else if (op.getEmbed() != null) {
                stringBuilder.insert(offset, Op.EMBED_TEXT_PLACEHOLDER);
                int length = Op.EMBED_TEXT_PLACEHOLDER.length();
                shift(offset, length);
                embedded.put(offset, op.getEmbed());
                return processAttributes(Range.between(offset, offset + length), op);
            }
        } else if (op.isDelete()) {
            int deleteLength = op.getDelete();
            int endOffset = offset + deleteLength;
            stringBuilder.delete(offset, endOffset);
            shift(offset + deleteLength, -deleteLength);
        }
        return offset;
    }

    private void shift(int offset, int length) {
        attributes.shift(offset, length);

        if (length > 0) {
            Set<Integer> embeddedOffsets = embedded.keySet();
            for (Integer embeddedOffset : embeddedOffsets) {
                if (embeddedOffset >= offset) {
                    Embed storedEmbed = embedded.remove(embeddedOffset);
                    embedded.put(embeddedOffset + length, storedEmbed);
                }
            }
        } else if (length < 0) {
            int endOffset = offset - length;
            for (int embeddedOffset = offset; embeddedOffset < endOffset; embeddedOffset++) {
                embedded.remove(embeddedOffset);
            }
            int len = endOffset - offset;
            Set<Integer> embeddedOffsets = embedded.keySet();
            for (Integer embeddedOffset : embeddedOffsets) {
                if (embeddedOffset >= endOffset) {
                    Embed storedEmbed = embedded.remove(embeddedOffset);
                    embedded.put(embeddedOffset - len, storedEmbed);
                }
            }
        }
    }

    private int processAttributes(Range range, Op op) {
        Set<Attribute> opAttributes = op.getAttributes();
        if (!opAttributes.isEmpty()) {
            attributes.splitRangeAttributesByRange(range);
            AttributeRanges targetedRanges = attributes.inRange(range);
            for (Attribute opAttribute : opAttributes) {
                AttributeType opAttributeType = opAttribute.getType();
                AttributeRanges opAttributeTypeRanges = targetedRanges.filterByAttributeType(opAttributeType);
                opAttributeTypeRanges.forEach(attributes::remove);
                if (!opAttribute.isDelete()) {
                    attributes.add(new AttributeRange(range, opAttribute));
                }
            }
        }
        return range.getEnd();
    }

}
