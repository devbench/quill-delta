/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import io.devbench.quilldelta.formatter.DeltaFormatter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public final class FormatterManager {

    private static final FormatterManager INSTANCE = new FormatterManager();

    private final Map<String, DeltaFormatter> formatters;

    private FormatterManager() {
        formatters = new HashMap<>();
        ServiceLoader<DeltaFormatter> loader = ServiceLoader.load(DeltaFormatter.class);
        for (DeltaFormatter formatter : loader) {
            registerFormatter(formatter);
        }
    }

    public static FormatterManager getInstance() {
        return INSTANCE;
    }

    public Collection<DeltaFormatter> getFormatters() {
        return Collections.unmodifiableCollection(formatters.values());
    }

    public Optional<DeltaFormatter> findFormatter(String formatterName) {
        return Optional.ofNullable(formatters.get(formatterName));
    }

    /**
     * Register a global delta formatter
     *
     * @param formatter the formatter to register
     * @return is the previously registered formatter with the exact same name, if exists, else null is returned
     */
    @Nullable
    public DeltaFormatter registerFormatter(@NotNull DeltaFormatter formatter) {
        return formatters.put(formatter.getFormatterName(), formatter);
    }

}
