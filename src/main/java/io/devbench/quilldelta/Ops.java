/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import elemental.json.*;
import io.devbench.quilldelta.embed.Embed;
import io.devbench.quilldelta.exception.DeltaOpException;
import org.jetbrains.annotations.NotNull;
import java.util.*;
import java.util.function.Function;

public class Ops extends ArrayList<Op> {

    private Map<Op, Range> offsetRangeMap = new HashMap<>();

    @Override
    public Op set(int index, Op element) {
        Op set = super.set(index, element);
        collectOffsetRangeMap();
        return set;
    }

    @Override
    public boolean add(Op op) {
        boolean add = super.add(op);
        collectOffsetRangeMap();
        return add;
    }

    @Override
    public void add(int index, Op element) {
        super.add(index, element);
        collectOffsetRangeMap();
    }

    @Override
    public boolean addAll(Collection<? extends Op> collection) {
        boolean listChanged = super.addAll(collection);
        collectOffsetRangeMap();
        return listChanged;
    }

    @Override
    public boolean addAll(int index, Collection<? extends Op> collection) {
        boolean listChanged = super.addAll(index, collection);
        collectOffsetRangeMap();
        return listChanged;
    }

    @Override
    public void sort(Comparator<? super Op> comparator) {
        throw new UnsupportedOperationException();
    }

    private void collectOffsetRangeMap() {
        int startOffset = 0;
        Map<Op, Range> rangeMap = new HashMap<>();
        for (Op op : this) {
            int endOffset = startOffset + (op.getInsert() != null ? op.getInsert().length() : 0);
            rangeMap.put(op, Range.between(startOffset, endOffset));
        }
        offsetRangeMap = rangeMap;
    }

    public Range getOpOffsetRange(Op op) {
        return offsetRangeMap.get(op);
    }

    private <R> Optional<R> withIndex(Op op, Function<Integer, R> function) {
        int index = indexOf(op);
        if (index != -1) {
            return Optional.ofNullable(function.apply(index));
        }
        return Optional.empty();
    }

    public boolean isLast(Op op) {
        return indexOf(op) == size() - 1;
    }

    public boolean replace(Op original, Op newOp) {
        return withIndex(original, index -> {
            set(index, newOp);
            return true;
        }).isPresent();
    }

    @NotNull
    public static Ops fromJson(@NotNull JsonObject jsonObject) {
        if (jsonObject.hasKey("ops")) {
            JsonArray jsonArray = jsonObject.getArray("ops");
            Ops ops = new Ops();
            for (int jsonIndex = 0; jsonIndex < jsonArray.length(); jsonIndex++) {
                JsonObject jsonOp = jsonArray.getObject(jsonIndex);
                Attribute[] attributes = jsonOp.hasKey("attributes") ? attributesFrom(jsonOp.getObject("attributes")) : new Attribute[0];
                if (jsonOp.hasKey("insert")) {
                    JsonValue insert = jsonOp.get("insert");
                    if (insert instanceof JsonString) {
                        String insertValue = insert.asString();
                        ops.add(Op.insert(insertValue, attributes));
                    } if (insert instanceof JsonObject) {
                        Embed embed = EmbedManager.getInstance().createEmbed((JsonObject) insert);
                        ops.add(Op.embed(embed, attributes));
                    }
                } else if (jsonOp.hasKey("retain")) {
                    int retain = (int) jsonOp.getNumber("retain");
                    ops.add(Op.retain(retain, attributes));
                } else if (jsonOp.hasKey("delete")) {
                    ops.add(Op.delete((int) jsonOp.getNumber("delete")));
                }
            }
            return ops;
        } else {
            throw new DeltaOpException("Cannot parse Ops from json object, no field named \"ops\": " + jsonObject.toJson());
        }
    }

    public JsonObject toJson() {
        JsonObject jsonObject = Json.createObject();
        JsonArray jsonOps = Json.createArray();
        for (int i = 0; i < size(); i++) {
            JsonObject jsonOp = Json.createObject();
            Op op = get(i);
            if (op.isInsert()) {
                JsonValue insertValue = null;
                String insert = op.getInsert();
                if (insert != null) {
                    insertValue = Json.create(insert);
                } else {
                    Embed embed = op.getEmbed();
                    if (embed != null) {
                        insertValue = EmbedManager.getInstance().createJson(embed);
                    }
                }
                if (insertValue != null) {
                    jsonOp.put("insert", insertValue);
                } else {
                    throw new DeltaOpException("Invalid insert, insert value is null for op: " + op);
                }
            } else if (op.isRetain()) {
                jsonOp.put("retain", op.getRetain());
            } else if (op.isDelete()) {
                jsonOp.put("delete", op.getDelete());
            } else {
                throw new DeltaOpException("Unsupported Operation: " + op);
            }
            if (!op.getAttributes().isEmpty()) {
                JsonObject attributes = Json.createObject();
                for (Attribute attribute : op.getAttributes()) {
                    JsonValue attributeValue;
                    if (attribute.isDelete()) {
                        attributeValue = Json.createNull();
                    } else {
                        AttributeValueType attributeValueType = attribute.getType().valueType();
                        switch (attributeValueType) {
                            case BOOL:
                                attributeValue = Json.create(attribute.getBool());
                                break;
                            case STRING:
                                attributeValue = Json.create(attribute.getString());
                                break;
                            case NUMBER:
                                attributeValue = Json.create(attribute.getNumber());
                                break;
                            default:
                                throw new DeltaOpException("Unsupported attribute value type: " + attributeValueType);
                        }
                    }
                    attributes.put(attribute.getType().getJsonName(), attributeValue);
                }
                jsonOp.put("attributes", attributes);
            }
            jsonOps.set(i, jsonOp);
        }
        jsonObject.put("ops", jsonOps);
        return jsonObject;
    }

    public static Ops from(Op... ops) {
        Ops theOps = new Ops();
        theOps.addAll(Arrays.asList(ops));
        return theOps;
    }

    private static Attribute[] attributesFrom(@NotNull JsonObject attributes) {
        List<Attribute> attributeList = new ArrayList<>();
        Arrays.stream(AttributeType.values())
            .map(AttributeType::getJsonName)
            .forEach(attributeJsonName ->
                withValue(attributes, attributeJsonName, jsonValue -> Attribute.from(attributeJsonName, jsonValue))
                    .ifPresent(attributeList::add));
        return attributeList.toArray(new Attribute[0]);
    }

    private static Optional<Attribute> withValue(@NotNull JsonObject jsonObject, @NotNull String key, Function<JsonValue, Attribute> jsonValueToAttribute) {
        if (jsonObject.hasKey(key)) {
            return Optional.ofNullable(jsonValueToAttribute.apply(jsonObject.get(key)));
        }
        return Optional.empty();
    }

}
