/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import io.devbench.quilldelta.embed.Embed;
import io.devbench.quilldelta.exception.DeltaOpException;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
@Builder
@ToString
@EqualsAndHashCode
public class Op implements Serializable {

    public static final String NL = "\n";
    public static final String EMBED_TEXT_PLACEHOLDER = " ";

    private final String insert;
    private final Integer delete;
    private final Integer retain;
    private final Embed embed;
    private final Set<Attribute> attributes;

    public boolean isNewLine() {
        return NL.equals(insert);
    }

    public OpType getType() {
        if (insert != null || embed != null) {
            return OpType.INSERT;
        } else if (retain != null) {
            return OpType.RETAIN;
        } else if (delete != null) {
            return OpType.DELETE;
        }
        throw new DeltaOpException("Unsupported Op type");
    }

    public boolean isInsert() {
        return getType() == OpType.INSERT;
    }

    public boolean isRetain() {
        return getType() == OpType.RETAIN;
    }

    public boolean isDelete() {
        return getType() == OpType.DELETE;
    }

    public static Op insert(String value, Attribute... attributes) {
        return Op.builder().insert(value).attributes(new HashSet<>(Arrays.asList(attributes))).build();
    }

    public static Op embed(Embed embed, Attribute... attributes) {
        return Op.builder().embed(embed).attributes(new HashSet<>(Arrays.asList(attributes))).build();
    }

    public static Op retain(int retain, Attribute... attributes) {
        return Op.builder().retain(retain).attributes(new HashSet<>(Arrays.asList(attributes))).build();
    }

    public static Op delete(int delete) {
        return Op.builder().delete(delete).build();
    }

}
