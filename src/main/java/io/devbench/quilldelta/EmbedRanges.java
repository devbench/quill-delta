/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta;

import io.devbench.quilldelta.embed.Embed;
import lombok.experimental.Delegate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmbedRanges implements Map<Integer, Embed> {

    @Delegate
    private final Map<Integer, Embed> embeddedRangesMap;

    public EmbedRanges() {
        this.embeddedRangesMap = new HashMap<>();
    }

    public List<Integer> sectionStartPoints() {
        return Stream.concat(
            embeddedRangesMap.keySet().stream(),
            embeddedRangesMap.keySet().stream().map(integer -> integer + Op.EMBED_TEXT_PLACEHOLDER.length()))
            .distinct()
            .sorted()
            .collect(Collectors.toList());
    }

}
