/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.formatter;

import io.devbench.quilldelta.Attribute;
import io.devbench.quilldelta.AttributeType;
import io.devbench.quilldelta.Op;
import io.devbench.quilldelta.Ops;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MarkdownFormatterTest extends BaseFormatterTest {

    private MarkdownFormatter testObj;

    @BeforeEach
    void setUp() {
        testObj = new MarkdownFormatter();
    }

    @Test
    @DisplayName("Bold test")
    void test_bold_test() {
        Ops ops = Ops.from(
            Op.insert("This is the first line\nSecond "),
            Op.insert("line", Attribute.create(AttributeType.BOLD, true)),
            Op.insert(" contains some bold text\n")
        );

        cycleTestFormatter("bold", testObj, ops, formattedText -> {
            String expectedFormattedText = "This is the first line  \nSecond **line** contains some bold text\n";
            assertEquals(expectedFormattedText, formattedText);
            return expectedFormattedText.equals(formattedText);
        });
    }

    @Test
    @DisplayName("Italic test")
    void test_italic_test() {
        Ops ops = Ops.from(
            Op.insert("This is the first line\nSecond "),
            Op.insert("line", Attribute.create(AttributeType.ITALIC, true)),
            Op.insert(" contains some bold text\n")
        );

        cycleTestFormatter("italic", testObj, ops, formattedText -> {
            String expectedFormattedText = "This is the first line  \nSecond *line* contains some bold text\n";
            assertEquals(expectedFormattedText, formattedText);
            return expectedFormattedText.equals(formattedText);
        });
    }

    @Test
    @DisplayName("Code block test")
    void test_code_block_test() {
        Ops ops = Ops.from(
            Op.insert("This is the "),
            Op.insert("first", Attribute.create(AttributeType.BOLD, true)),
            Op.insert(" line.\nthis is a code block"),
            Op.insert("\n", Attribute.create(AttributeType.CODE_BLOCK, true)),
            Op.insert("with some **formatting** which must be left untouched"),
            Op.insert("\n", Attribute.create(AttributeType.CODE_BLOCK, true)),
            Op.insert("the end")
        );

        cycleTestFormatter("code block", testObj, ops, formattedText -> {
            String expectedFormattedText = "This is the **first** line.  \n" +
                "```\n" +
                "this is a code block\nwith some **formatting** which must be left untouched\n" +
                "```\n" +
                "the end\n";
            assertEquals(expectedFormattedText, formattedText);
            return expectedFormattedText.equals(formattedText);
        });
    }

}
