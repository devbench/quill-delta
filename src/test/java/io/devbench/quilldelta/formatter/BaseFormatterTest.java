/*
 *
 * Copyright © 2021 Webvalto Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.devbench.quilldelta.formatter;

import io.devbench.quilldelta.Delta;
import io.devbench.quilldelta.Op;
import io.devbench.quilldelta.Ops;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

abstract class BaseFormatterTest {

    protected void cycleTestFormatter(String testName, DeltaFormatter testObj, Ops ops, Function<String, Boolean> formattedTest) {
        Delta delta = new Delta(ops);
        Op[] originalDeltaOps = delta.getOps().toArray(new Op[0]);

        String formatted = testObj.render(delta);
        Boolean formattedTestResult = formattedTest.apply(formatted);
        if (formattedTestResult) {
            Delta parsedDelta = testObj.parse(formatted);
            Op[] parsedDeltaOps = parsedDelta.getOps().toArray(new Op[0]);
            assertArrayEquals(originalDeltaOps, parsedDeltaOps, "Delta ops are not equal for test " + testName);
        } else {
            fail("Formatted test failed for test " + testName);
        }
    }

}
